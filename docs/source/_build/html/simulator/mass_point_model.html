<!DOCTYPE html>
<html class="writer-html5" lang="en" >
<head>
  <meta charset="utf-8" /><meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Mass Point Model &mdash; PSimPy&#39;s documentation</title>
      <link rel="stylesheet" href="../_static/pygments.css" type="text/css" />
      <link rel="stylesheet" href="../_static/css/theme.css" type="text/css" />
      <link rel="stylesheet" href="../_static/sg_gallery.css" type="text/css" />
      <link rel="stylesheet" href="../_static/sg_gallery-binder.css" type="text/css" />
      <link rel="stylesheet" href="../_static/sg_gallery-dataframe.css" type="text/css" />
      <link rel="stylesheet" href="../_static/sg_gallery-rendered-html.css" type="text/css" />
      <link rel="stylesheet" href="../_static/css/custom.css" type="text/css" />
  <!--[if lt IE 9]>
    <script src="../_static/js/html5shiv.min.js"></script>
  <![endif]-->
  
        <script data-url_root="../" id="documentation_options" src="../_static/documentation_options.js"></script>
        <script src="../_static/jquery.js"></script>
        <script src="../_static/underscore.js"></script>
        <script src="../_static/_sphinx_javascript_frameworks_compat.js"></script>
        <script src="../_static/doctools.js"></script>
        <script src="../_static/sphinx_highlight.js"></script>
        <script async="async" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <script src="../_static/js/theme.js"></script>
    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="Ravaflow24 Mixture Model" href="ravaflow24.html" />
    <link rel="prev" title="Run Simulator" href="run_simulator.html" /> 
</head>

<body class="wy-body-for-nav"> 
  <div class="wy-grid-for-nav">
    <nav data-toggle="wy-nav-shift" class="wy-nav-side">
      <div class="wy-side-scroll">
        <div class="wy-side-nav-search" >
            <a href="../index.html" class="icon icon-home"> psimpy
          </a>
              <div class="version">
                0.1.2
              </div>
<div role="search">
  <form id="rtd-search-form" class="wy-form" action="../search.html" method="get">
    <input type="text" name="q" placeholder="Search docs" />
    <input type="hidden" name="check_keywords" value="yes" />
    <input type="hidden" name="area" value="default" />
  </form>
</div>
        </div><div class="wy-menu wy-menu-vertical" data-spy="affix" role="navigation" aria-label="Navigation menu">
              <ul class="current">
<li class="toctree-l1"><a class="reference internal" href="../index.html">Home</a></li>
<li class="toctree-l1"><a class="reference internal" href="../quickstart.html">Getting Started</a></li>
<li class="toctree-l1 current"><a class="reference internal" href="../api.html">API &amp; Theory</a><ul class="current">
<li class="toctree-l2"><a class="reference internal" href="../emulator/index.html">Emulator</a></li>
<li class="toctree-l2"><a class="reference internal" href="../inference/index.html">Inference</a></li>
<li class="toctree-l2"><a class="reference internal" href="../sampler/index.html">Sampler</a></li>
<li class="toctree-l2"><a class="reference internal" href="../sensitivity/index.html">Sensitivity</a></li>
<li class="toctree-l2 current"><a class="reference internal" href="index.html">Simulator</a><ul class="current">
<li class="toctree-l3"><a class="reference internal" href="run_simulator.html"> Run Simulator</a></li>
<li class="toctree-l3 current"><a class="current reference internal" href="#"> Mass Point Model</a><ul>
<li class="toctree-l4"><a class="reference internal" href="#theory-of-mass-point-model">Theory of mass point model</a></li>
<li class="toctree-l4"><a class="reference internal" href="#masspointmodel-class">MassPointModel Class</a></li>
</ul>
</li>
<li class="toctree-l3"><a class="reference internal" href="ravaflow24.html"> Ravaflow Mixture Model</a></li>
</ul>
</li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="../examples.html">Example Gallery</a></li>
<li class="toctree-l1"><a class="reference internal" href="../changelog.html">Changes</a></li>
<li class="toctree-l1"><a class="reference internal" href="../refs.html">References</a></li>
</ul>

        </div>
      </div>
    </nav>

    <section data-toggle="wy-nav-shift" class="wy-nav-content-wrap"><nav class="wy-nav-top" aria-label="Mobile navigation menu" >
          <i data-toggle="wy-nav-top" class="fa fa-bars"></i>
          <a href="../index.html">psimpy</a>
      </nav>

      <div class="wy-nav-content">
        <div class="rst-content">
          <div role="navigation" aria-label="Page navigation">
  <ul class="wy-breadcrumbs">
      <li><a href="../index.html" class="icon icon-home"></a></li>
          <li class="breadcrumb-item"><a href="../api.html">API &amp; Theory</a></li>
          <li class="breadcrumb-item"><a href="index.html">Simulator</a></li>
      <li class="breadcrumb-item active">Mass Point Model</li>
      <li class="wy-breadcrumbs-aside">
            <a href="../_sources/simulator/mass_point_model.rst.txt" rel="nofollow"> View page source</a>
      </li>
  </ul>
  <hr/>
</div>
          <div role="main" class="document" itemscope="itemscope" itemtype="http://schema.org/Article">
           <div itemprop="articleBody">
             
  <section id="mass-point-model">
<h1>Mass Point Model<a class="headerlink" href="#mass-point-model" title="Permalink to this heading"></a></h1>
<section id="theory-of-mass-point-model">
<h2>Theory of mass point model<a class="headerlink" href="#theory-of-mass-point-model" title="Permalink to this heading"></a></h2>
<p>A <cite>mass point model</cite> is an extremely simplified model to simulate mass movement
on a given topography. It assumes that the flow mass is condensed to a single point.</p>
<p>Let <span class="math notranslate nohighlight">\(Z(x,y)\)</span> define a topography in a Cartesian
coordinate system <span class="math notranslate nohighlight">\(\{x, y, z\}\)</span>. It induces a local non-orthogonal
coordinate system <span class="math notranslate nohighlight">\(\{T_x, T_y, T_n\}\)</span>, where <span class="math notranslate nohighlight">\(T_x\)</span> and <span class="math notranslate nohighlight">\(T_y\)</span>
denote surface tangent directions and <span class="math notranslate nohighlight">\(T_n\)</span> denote surface normal direction
<span class="math notranslate nohighlight">\(T_n\)</span>, as shown by the following figure <span id="id1">[<a class="reference internal" href="../refs.html#id15" title="Hu Zhao. Gaussian processes for sensitivity analysis, Bayesian inference, and uncertainty quantification in landslide research. PhD thesis, RWTH Aachen University, 2021. doi:10.18154/RWTH-2021-11693.">Zhao, 2021</a>]</span></p>
<a class="reference internal image-reference" href="../_images/coordinate_system.png"><img alt="a surface-induced coordinate system" class="align-center" src="../_images/coordinate_system.png" style="width: 300px;" /></a>
<p>The govening equations describing the movement of a masspoint on the surface are
given by</p>
<div class="math notranslate nohighlight" id="equation-dxdt">
<span class="eqno">(1)<a class="headerlink" href="#equation-dxdt" title="Permalink to this equation"></a></span>\[\frac{d x}{d t} = U_{T_x} \frac{1}{\sqrt{1+\left(\partial_x Z(x, y)\right)^2}}\]</div>
<div class="math notranslate nohighlight" id="equation-dydt">
<span class="eqno">(2)<a class="headerlink" href="#equation-dydt" title="Permalink to this equation"></a></span>\[\frac{d y}{d t} = U_{T_y} \frac{1}{\sqrt{1+\left(\partial_y Z(x, y)\right)^2}}\]</div>
<div class="math notranslate nohighlight" id="equation-dutxdt">
<span class="eqno">(3)<a class="headerlink" href="#equation-dutxdt" title="Permalink to this equation"></a></span>\[\frac{d U_{T_x}}{d t}=g_{T_x} - \frac{U_{T_x}}{\|\boldsymbol{U}\|}
\cdot \left(\mu\left(g_N+\boldsymbol{U}^T \boldsymbol{K} \boldsymbol{U}\right) +
\frac{g}{\xi}\|\boldsymbol{U}\|^2\right)\]</div>
<div class="math notranslate nohighlight" id="equation-dutydt">
<span class="eqno">(4)<a class="headerlink" href="#equation-dutydt" title="Permalink to this equation"></a></span>\[\frac{d U_{T_y}}{d t} = g_{T_y} - \frac{U_{T_y}}{\|\boldsymbol{U}\|}
\cdot\left(\mu\left(g_N+\boldsymbol{U}^T \boldsymbol{K} \boldsymbol{U}\right) +
\frac{g}{\xi}\|\boldsymbol{U}\|^2\right)\]</div>
<p>where <span class="math notranslate nohighlight">\(\mu\)</span> and <span class="math notranslate nohighlight">\(\xi\)</span> are dry-Coulomb and turbulent friction coefficient
respectively (Voellmy friction model is used here). <span class="math notranslate nohighlight">\(\mathbf{K}\)</span> is the
curvature tensor <span id="id2">[<a class="reference internal" href="../refs.html#id3" title="Jan-Thomas Fischer, Julia Kowalski, and Shiva P. Pudasaini. Topographic curvature effects in applied avalanche modeling. Cold Regions Science and Technology, 74-75:21–30, 2012. doi:https://doi.org/10.1016/j.coldregions.2012.01.005.">Fischer <em>et al.</em>, 2012</a>]</span>. <span class="math notranslate nohighlight">\(\mathbf{U}\)</span> represents the masspoint’s
velocity. <span class="math notranslate nohighlight">\(U_{T_x}\)</span> and <span class="math notranslate nohighlight">\(U_{T_y}\)</span> are the velocity components along <span class="math notranslate nohighlight">\(T_x\)</span>
and <span class="math notranslate nohighlight">\(T_y\)</span> direction respectively.</p>
<p>Equations <a class="reference internal" href="#equation-dxdt">(1)</a> to <a class="reference internal" href="#equation-dutydt">(4)</a> can be rewritten in the vector format as</p>
<div class="math notranslate nohighlight" id="equation-dalphadt">
<span class="eqno">(5)<a class="headerlink" href="#equation-dalphadt" title="Permalink to this equation"></a></span>\[\frac{d \boldsymbol{\alpha}}{d t}=\boldsymbol{f}(t, \boldsymbol{\alpha})\]</div>
<div class="math notranslate nohighlight" id="equation-alpha">
<span class="eqno">(6)<a class="headerlink" href="#equation-alpha" title="Permalink to this equation"></a></span>\[\begin{split}\boldsymbol{\alpha}=\left[\begin{array}{c}
x(t) \\
y(t) \\
U_{T_x}(t) \\
U_{T_y}(t)
\end{array}\right]\end{split}\]</div>
<div class="math notranslate nohighlight" id="equation-vector-f">
<span class="eqno">(7)<a class="headerlink" href="#equation-vector-f" title="Permalink to this equation"></a></span>\[\begin{split}\boldsymbol{f}=\left[\begin{array}{c}
U_{T_x} \frac{1}{\sqrt{1+\left(\partial_x Z(x, y)^2\right.}} \\
U_{T_y} \frac{1}{\sqrt{1+\left(\partial_y Z(x, y)^2\right.}} \\
g_{T_x}-\frac{U_{T_x}}{\|\boldsymbol{U}\|} \cdot\left(\mu\left(g_N+\boldsymbol{U}^T
\boldsymbol{K} \boldsymbol{U}\right)+\frac{g}{\xi}\|\boldsymbol{U}\|^2\right) \\
g_{T_y}-\frac{U_{T_y}}{\|\boldsymbol{U}\|} \cdot\left(\mu\left(g_N+\boldsymbol{U}^T
\boldsymbol{K} \boldsymbol{U}\right)+\frac{g}{\xi}\|\boldsymbol{U}\|^2\right)
\end{array}\right]\end{split}\]</div>
<p>Equation <a class="reference internal" href="#equation-dalphadt">(5)</a> defines an initial value problem. Given initial
<span class="math notranslate nohighlight">\(\boldsymbol{\alpha}_0\)</span>, the system can be solved forward in time using
numerical schemes such as the runge-kutta method. Class <a class="reference internal" href="#psimpy.simulator.mass_point_model.MassPointModel" title="psimpy.simulator.mass_point_model.MassPointModel"><code class="xref py py-class docutils literal notranslate"><span class="pre">MassPointModel</span></code></a>
utilizes the explicit runge-kutta method <code class="docutils literal notranslate"><span class="pre">dopri5</span></code> provided by
<code class="xref py py-class docutils literal notranslate"><span class="pre">scipy.integrate.ode</span></code>.</p>
</section>
<section id="masspointmodel-class">
<h2>MassPointModel Class<a class="headerlink" href="#masspointmodel-class" title="Permalink to this heading"></a></h2>
<p>The <a class="reference internal" href="#psimpy.simulator.mass_point_model.MassPointModel" title="psimpy.simulator.mass_point_model.MassPointModel"><code class="xref py py-class docutils literal notranslate"><span class="pre">MassPointModel</span></code></a> class is imported by:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="kn">from</span> <span class="nn">psimpy.simulator.mass_point_model</span> <span class="kn">import</span> <span class="n">MassPointModel</span>
</pre></div>
</div>
<section id="methods">
<h3>Methods<a class="headerlink" href="#methods" title="Permalink to this heading"></a></h3>
<dl class="py class">
<dt class="sig sig-object py" id="psimpy.simulator.mass_point_model.MassPointModel">
<em class="property"><span class="pre">class</span><span class="w"> </span></em><span class="sig-name descname"><span class="pre">MassPointModel</span></span><a class="reference internal" href="../_modules/psimpy/simulator/mass_point_model.html#MassPointModel"><span class="viewcode-link"><span class="pre">[source]</span></span></a><a class="headerlink" href="#psimpy.simulator.mass_point_model.MassPointModel" title="Permalink to this definition"></a></dt>
<dd><p>Simulate the movement of a masspoint on a topography.</p>
<dl class="py method">
<dt class="sig sig-object py" id="psimpy.simulator.mass_point_model.MassPointModel.run">
<span class="sig-name descname"><span class="pre">run</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">elevation</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">coulomb_friction</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">turbulent_friction</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">x0</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">y0</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">ux0</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">0</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">uy0</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">0</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">dt</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">1</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">tend</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">300</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">t0</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">0</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">g</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">9.8</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">atol</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">1e-06</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">rtol</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">1e-06</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">curvature</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">False</span></span></em><span class="sig-paren">)</span><a class="reference internal" href="../_modules/psimpy/simulator/mass_point_model.html#MassPointModel.run"><span class="viewcode-link"><span class="pre">[source]</span></span></a><a class="headerlink" href="#psimpy.simulator.mass_point_model.MassPointModel.run" title="Permalink to this definition"></a></dt>
<dd><p>Solve the mass point model using <code class="xref py py-class docutils literal notranslate"><span class="pre">scipy.integrate.ode</span></code> solver
given required input data.</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><ul class="simple">
<li><p><strong>elevation</strong> (<em>str</em>) – Name of the elevation raster file, in ESRI ascii format, including
the path.</p></li>
<li><p><strong>coulomb_friction</strong> (<em>float</em>) – Dry Coulomb friction coefficient.</p></li>
<li><p><strong>turbulent_friction</strong> (<em>float</em><em> or </em><em>int</em>) – Turbulent friction coefficient, in <span class="math notranslate nohighlight">\(m/s^2\)</span>.</p></li>
<li><p><strong>x0</strong> (<em>float</em><em> or </em><em>int</em>) – <cite>x</cite> coordinate of initial position.</p></li>
<li><p><strong>y0</strong> (<em>float</em><em> or </em><em>int</em>) – <cite>y</cite> coordinate of initial position.</p></li>
<li><p><strong>ux0</strong> (<em>float</em><em> or </em><em>int</em>) – Initial velocity in <cite>x</cite> direction.</p></li>
<li><p><strong>uy0</strong> (<em>float</em><em> or </em><em>int</em>) – Initial velocity in <cite>y</cite> direction.</p></li>
<li><p><strong>dt</strong> (<em>float</em><em> or </em><em>int</em>) – Time step in seconds.</p></li>
<li><p><strong>tend</strong> (<em>float</em><em> or </em><em>int</em>) – End time in seconds.</p></li>
<li><p><strong>t0</strong> (<em>float</em><em> or </em><em>int</em>) – Initial time.</p></li>
<li><p><strong>g</strong> (<em>float</em><em> or </em><em>int</em>) – Gravity acceleration, in <span class="math notranslate nohighlight">\(m/s^2\)</span>.</p></li>
<li><p><strong>atol</strong> (<em>float</em>) – Absolute tolerance for solution.</p></li>
<li><p><strong>rtol</strong> (<em>float</em>) – Relative tolerance for solution.</p></li>
<li><p><strong>curvature</strong> (<em>bool</em>) – If <cite>True</cite>, take the curvature effect into account.</p></li>
</ul>
</dd>
<dt class="field-even">Returns</dt>
<dd class="field-even"><p><strong>output</strong> – Time history of the mass point’s location and velocity.
2d <code class="xref py py-class docutils literal notranslate"><span class="pre">numpy.ndarray</span></code>. Each row corresponds to a time step and
each column corresponds to a quantity. In total <span class="math notranslate nohighlight">\(6\)</span> columns,
namely <code class="code docutils literal notranslate"><span class="pre">output.shape[1]=6</span></code>. More specifically:
<code class="code docutils literal notranslate"><span class="pre">output[:,0]</span></code> are the time steps,
<code class="code docutils literal notranslate"><span class="pre">output[:,1]</span></code> are the <cite>x</cite> coordinates,
<code class="code docutils literal notranslate"><span class="pre">output[:,2]</span></code> are the <cite>y</cite> coordinates,
<code class="code docutils literal notranslate"><span class="pre">output[:,3]</span></code> are velocity values in <cite>x</cite> direction,
<code class="code docutils literal notranslate"><span class="pre">output[:,4]</span></code> are velocity values in <cite>y</cite> direction,
<code class="code docutils literal notranslate"><span class="pre">output[:,5]</span></code> are total velocity values.</p>
</dd>
<dt class="field-odd">Return type</dt>
<dd class="field-odd"><p>numpy array</p>
</dd>
</dl>
</dd></dl>

</dd></dl>

</section>
</section>
</section>


           </div>
          </div>
          <footer><div class="rst-footer-buttons" role="navigation" aria-label="Footer">
        <a href="run_simulator.html" class="btn btn-neutral float-left" title="Run Simulator" accesskey="p" rel="prev"><span class="fa fa-arrow-circle-left" aria-hidden="true"></span> Previous</a>
        <a href="ravaflow24.html" class="btn btn-neutral float-right" title="Ravaflow24 Mixture Model" accesskey="n" rel="next">Next <span class="fa fa-arrow-circle-right" aria-hidden="true"></span></a>
    </div>

  <hr/>

  <div role="contentinfo">
    <p>&#169; Copyright 2023, Hu Zhao.</p>
  </div>

  Built with <a href="https://www.sphinx-doc.org/">Sphinx</a> using a
    <a href="https://github.com/readthedocs/sphinx_rtd_theme">theme</a>
    provided by <a href="https://readthedocs.org">Read the Docs</a>.
   

</footer>
        </div>
      </div>
    </section>
  </div>
  <script>
      jQuery(function () {
          SphinxRtdTheme.Navigation.enable(true);
      });
  </script> 

</body>
</html>