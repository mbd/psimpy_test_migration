{
  "cells": [
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "%matplotlib inline"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n# ScalarGaSP: GP emulation for a single-output function\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "This example shows how to apply Gaussian process emulation to a single-output\nfunction using class :class:`.ScalarGaSP`.\n\nThe task is to build a GP emulator for the function $y = x * sin(x)$\nbased on a few number of training data.\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "First, import the class :class:`.ScalarGaSP` by\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "from psimpy.emulator import ScalarGaSP"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Then, create an instance of :class:`.ScalarGaSP`.  The parameter ``ndim``\n(dimension of function input ``x``) must be specified. Optional parameters, such\nas ``method``, ``kernel_type``, etc., can be set up if desired. Here, we leave\nall the optional parameters to their default values.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "emulator = ScalarGaSP(ndim=1)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Given training input points ``design`` and corresponding output values ``response``,\nthe emulator can be trained using :py:meth:`.ScalarGaSP.train`. Below we train\nan emulator using $8$ selected points.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import numpy as np\n\ndef f(x):\n    #return x + 3*np.sin(x/2)\n    return x*np.sin(x)\n\nx = np.arange(2,10,1)\ny = f(x)\n\nemulator.train(design=x, response=y)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "We can validate the performance of the trained emulator using the leave-one-out\ncross validation method :py:meth:`loo_validate()`.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "validation = emulator.loo_validate()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Let's plot emulator predictions vs actual outputs. The error bar indicates the\nstandard deviation.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import matplotlib.pyplot as plt\n\nfig , ax = plt.subplots(figsize=(4,4))\n\nax.set_xlabel('Actual y')\nax.set_ylabel('Emulator-predicted y')\nax.set_xlim(np.min(y)-1,np.max(y)+1)\nax.set_ylim(np.min(y)-1,np.max(y)+1)\n\n_ = ax.plot([np.min(y)-1,np.max(y)+1], [np.min(y)-1,np.max(y)+1])\n_ = ax.errorbar(y, validation[:,0], validation[:,1], fmt='.', linestyle='', label='prediction and std')\n_ = plt.legend()\nplt.tight_layout()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "With the trained emulator at our deposit, we can use the\n:py:meth:`.ScalarGaSP.predict()` method to make predictions at \nany arbitrary set of input points (``testing_input``). It should be noted that,\n``testing_trend`` should be set according to ``trend`` used during emulator\ntraining. \n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "testing_input = np.arange(0,10,0.1)\npredictions = emulator.predict(testing_input)\n\nplt.plot(testing_input, predictions[:, 0], 'r-', label= \"mean\")\nplt.scatter(x, y, s=15, c='k', label=\"training data\", zorder=3)\nplt.plot(testing_input, f(testing_input), 'k:', zorder=2, label=\"true function\")\nplt.fill_between(testing_input, predictions[:, 1], predictions[:, 2], alpha=0.3, label=\"95% CI\")\nplt.xlabel('x')\nplt.ylabel('emulator-predicted y')\nplt.xlim(testing_input[0], testing_input[-1])\n_ = plt.legend()\nplt.tight_layout()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "We can also draw any number of samples at ``testing_input``` using\n:py:meth:`.ScalarGaSP.sample()` method.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "nsamples = 5\nsamples = emulator.sample(testing_input, nsamples=nsamples)\n\n# sphinx_gallery_thumbnail_number = 3\nfor i in range(nsamples):\n    plt.plot(testing_input, samples[:,i], '--', label=f'sample{i+1}')\n\nplt.scatter(x, y, s=15, c='k', label=\"training data\", zorder=2)\nplt.plot(testing_input, f(testing_input), 'k:', zorder=2, label=\"true function\")\nplt.fill_between(testing_input, predictions[:, 1], predictions[:, 2], alpha=0.3, label=\"95% CI\")\nplt.xlabel('x')\nplt.ylabel('emulator-predicted y')\nplt.xlim(testing_input[0], testing_input[-1])\n_ = plt.legend()\nplt.tight_layout()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        ".. tip:: Above example shows how to train a GP emulator based on noise-free training data,\n   which is often the case of emulating a deterministic simulator. If you are dealing\n   with noisy training data, you can\n\n    - set the parameter ``nugget`` to a desired value, or\n    - set ``nugget`` to $0$ and ``nugget_est`` to `True`, meaning that ``nugget``\n      is estimated from the noisy training data.\n"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.9.12"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}