{
  "cells": [
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "%matplotlib inline"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n# Active learning\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "This example shows how to use the :class:`.ActiveLearning` class to iteratively\nbuild a Gaussian process emulator for an unnormalized posterior involving a\nsimulator. It should be noted that this example is only for illustration\npurpose, rather than a real case. For simplicity, required arguments for\n:class:`.ActiveLearning` (including `simulator`, `likelihood`, `data`, etc.) are purely\nmade up. For a realistic case of active learning, one can refer to :cite:t:`Zhao2022`.\n\nFirst, we define the simulator, prior distribution of its variable parameters,\nobserved data, and likelihood function. They basically define the Bayesian\ninference problem. \n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import numpy as np\n\nndim = 2 # dimension of variable parameters of the simulator\nbounds = bounds = np.array([[-5,5],[-5,5]]) # bounds of variable parameters\ndata = np.array([1,0])\n\ndef simulator(x1, x2):\n    \"\"\"Simulator y=f(x).\"\"\"\n    y1, y2 = x1, x2\n    return np.array([y1, y2])\n\ndef prior(x):\n    \"\"\"Uniform prior.\"\"\"\n    return 1/(10*10)\n\ndef likelihood(y, data):\n    \"\"\"Likelihood function L(y,data).\"\"\"\n    return np.exp(-(y[0]-data[0])**2/100 - (y[0]**2-y[1]-data[1])**2)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Imagine that the simulator is a complex solver. It is not computationally feasible\nto compute the posterior distribution of the variable parameters using grid estimation\nor Metropolis Hastings estimation. This is because they require evaluating the likelihood\nmany times which essentially leads to many evaluations of the simulator. Therefore, we\nresort to use active learning to build a Gaussian process emulator for the unnormalized\nposterior (prior times likelihood) based on a small number of evaluations of the simulator.\nThe the posterior can be estimated using the emulator.\n\nTo do so, we need to pass arguments to following parameters of the :class:`.ActiveLearning` class:\n\n  - run_sim_obj : instance of class :class:`.RunSimulator`. It carries information on how\n    to run the simulator.\n  - lhs_sampler : instance of class :class:`.LHS`. It is used to draw initial samples to run\n    simulations in order to train an inital Gaussian process emulator.\n  - scalar_gasp : instance of class :class:`.ScalarGaSP`. It sets up the emulator structure.\n\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "from psimpy.simulator import RunSimulator\nfrom psimpy.sampler import LHS\nfrom psimpy.emulator import ScalarGaSP\n\nrun_simulator = RunSimulator(simulator, var_inp_parameter=['x1','x2'])\nlhs_sampler = LHS(ndim=ndim, bounds=bounds, seed=1)\nscalar_gasp = ScalarGaSP(ndim=ndim)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Next, we create an object of the :class:`.ActiveLearning` class by\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "from psimpy.inference import ActiveLearning\n\nactive_learner = ActiveLearning(ndim, bounds, data, run_simulator, prior, likelihood,\n    lhs_sampler, scalar_gasp)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Then we can call the :py:meth:`.ActiveLearning.initial_simulation` method to run initial\nsimulations and call the :py:meth:`.ActiveLearning.iterative_emulation` method to\niteratively run new simulation and build emulator. Here we allocate 40 simulations for\ninitial emulator training and 60 simulations for adaptive training.\n\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "n0 = 40\nniter = 60\n\ninit_var_samples, init_sim_outputs = active_learner.initial_simulation(\n    n0, mode='parallel', max_workers=4)\n\nvar_samples, _, _ = active_learner.iterative_emulation(\n    n0, init_var_samples, init_sim_outputs, niter=niter)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Once the active learning process is finished, we obtain the final emulator for the\nlogarithm of the unnormalized posterior, which is given by the\n:py:meth:`.ActiveLearning.approx_ln_pxl` method.\n\nWe can then estimate the posterior using grid estimation or Metropolis Hastings\nestimation based on the emulator. An example is as follows. The contour plot shows\nthe estimated posterior. \n\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "from psimpy.inference import GridEstimation\nimport matplotlib.pyplot as plt\n\ngrid_estimator = GridEstimation(ndim, bounds, ln_pxl=active_learner.approx_ln_pxl)\nposterior, x_ndim = grid_estimator.run(nbins=50)\n\nfig, ax = plt.subplots(1,1,figsize=(5,4))\n\n# initial training points\nax.scatter(init_var_samples[:,0], init_var_samples[:,1], s=10, c='r', marker='o',\n    zorder=1, alpha=0.8, label='initial training points')\n# actively picked training points\nax.scatter(var_samples[n0:,0], var_samples[n0:,1], s=15, c='k', marker='+',\n    zorder=2, alpha=0.8, label='iterative training points')\n\n# estimated posterior based on the final emulator\nposterior = np.where(posterior < 1e-10, None, posterior)\ncontour = ax.contour(x_ndim[0], x_ndim[1], np.transpose(posterior), levels=10, zorder=0)\nplt.colorbar(contour, ax=ax)\n\nax.legend()\nax.set_title('Active learning')\nax.set_xlabel('x1')\nax.set_ylabel('x2')\nax.set_xlim([-5,5])\nax.set_ylim([-5,5])\nplt.tight_layout()"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.9.12"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}