{
  "cells": [
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "%matplotlib inline"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n# Sobol' analysis\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "This example shows how to compute Sobol' sensitivity indices using\nthe :class:`.SobolAnalyze` class. \n\nLet us consider the Ishigami function :cite:p:`LeGratiet2014`\n\n$y=f\\left(x_1, x_2, x_3\\right)=\\sin \\left(x_1\\right)+7 \\sin \\left(\\mathrm{x}_2\\right)^2+0.1 x_3^4 \\sin \\left(x_1\\right)$\n\nwhere $x_i$ is uniformly distributed on $[-\\pi, \\pi]$ for $i \\in [1,2,3]$. We define the function as follows:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import numpy as np\n\ndef f(x1,x2,x3):\n    return np.sin(x1) + 7*np.sin(x2)**2 + 0.1*x3**4*np.sin(x1)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The variation of each $x_i$ leads to the variation of $y$. Sobol' analysis is used to quantify the contribution\nof each $x_i$ and their interactions to the variation of $y$. Steps of a Sobol' analysis are:\n\n #. Draw samples of input.\n #. Evaluate the model at each input sample to obtain model output.\n #. Compute Sobol' indices based on model outputs.\n\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "from psimpy.sampler import Saltelli\nfrom psimpy.sensitivity import SobolAnalyze\n\nndim = 3\nbounds = np.array([[-np.pi, np.pi], [-np.pi, np.pi], [-np.pi, np.pi]])\ncalc_second_order = False\nnbase = 1024\n\n# draw Saltelli samples\nsaltelli_sampler = Saltelli(ndim, bounds, calc_second_order)\nsaltelli_samples = saltelli_sampler.sample(nbase)\n\n# Evaluate the model to obtain model outputs\nY = f(saltelli_samples[:,0], saltelli_samples[:,1], saltelli_samples[:, 2])\n\n# Compute Sobol' indices\nsobol_analyzer = SobolAnalyze(ndim, Y, calc_second_order, seed=2)\nS_res = sobol_analyzer.run()\n\nprint(\"Estimated first-order Sobol' index: \", S_res['S1'][:,0])\nprint(\"Analytical solution: [0.314, 0.442, 0]\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Figure below shows the results. The bar plots represent estimated first-order\nand total-effect Sobol' indices, as well as their 95% confidence interval. The\nred stars represent true values of the first-order Sobol' indices.\n\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import matplotlib.pyplot as plt\n\nname_vars = [r'$x_1$',r'$x_2$',r'$x_3$']\nx = {'S1':[1,4,7], 'ST':[2,5,8]}\ncolor = {'S1':'#69b3a2', 'ST':'#3399e6'}\nwidth=0.8\ncapsize=3\n\nfig, ax1 = plt.subplots(figsize=(4,3))\n\n# estimated first-order Sobol' indices and their confidence interval\nax1.bar(x['S1'], S_res['S1'][:,0], width=width, yerr=S_res['S1'][:,2], color=color['S1'], capsize=capsize)\n\nax1.axis([0,9,0,0.7])\nax1.set_xticks([1.5,4.5,7.5])\nax1.set_xticklabels(name_vars)\nax1.tick_params(axis='both',bottom=False)\nax1.tick_params(axis=\"y\", labelcolor=color['S1'])\nax1.yaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)\nax1.set_ylabel('First-order index', color=color['S1'])\n\n# analytical solution for first-order indices\nax1.scatter(x['S1'], [0.314, 0.442, 0], s=30, c='red', marker='*')\n\n\n# estimated total-effect Sobol' indices and their confidence interval\nax2 = ax1.twinx()\nax2.bar(x['ST'], S_res['ST'][:,0], width=width, yerr=S_res['ST'][:,2], color=color['ST'], capsize=capsize)\n\nax2.axis([0,9,0,0.7])\nax2.set_xticks([1.5,4.5,7.5])\nax2.tick_params(axis='both',bottom=False)\nax2.tick_params(axis=\"y\", labelcolor=color['ST'])\nax2.yaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)\nax2.set_ylabel('Total-effect index', color=color['ST'])\n\nplt.tight_layout()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "<div class=\"alert alert-info\"><h4>Note</h4><p>The interaction between one parameter and all other parameters can be\n    quantified by the difference between its first-order and total-effet Sobol'\n    indices. If detailed information on interaction between any two parameters\n    is needed, one can set ``calc_second_order`` to `True`.</p></div>\n\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "**Many models of interest in real-world applications are very computationally\nexpensive. In that case, we resort to emulation techniques in order to efficiently\nconduct Sobol' analysis.** See :cite:t:`Zhao2021a` for the detailed theory.\n\nSteps of such an analysis are:\n\n #. Draw input samples for emulator training.\n #. Evaluate the model at each training input point to obtain training data.\n #. Train an emulator for the computationally expensive model of interest.\n #. Draw samples of input for Sobol' analysis.\n #. Evaluate the emulator at each input sample at step 3 to obtain approximated model outputs.\n #. Compute Sobol' indices based on approximated model outputs.\n\nAssume that the above Ishigami function is computationally expensive, an emulator-based\nSobol' analysis can be conducted as follows:\n\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import numpy as np\nfrom psimpy.sampler import LHS\nfrom psimpy.emulator import ScalarGaSP\nfrom psimpy.sampler import Saltelli\nfrom psimpy.sensitivity import SobolAnalyze\n\n# define the model\ndef f(x1,x2,x3):\n    return np.sin(x1) + 7*np.sin(x2)**2 + 0.1*x3**4*np.sin(x1)\n\nndim = 3\nbounds = np.array([[-np.pi, np.pi], [-np.pi, np.pi], [-np.pi, np.pi]])\nseed = 2\n\n# draw 100 input samples for emulator training using Latin hypercube sampling\nlhs_sampler = LHS(ndim, bounds, seed, criterion='maximin', iteration=200)\ndesign = lhs_sampler.sample(nsamples=100)\n\n# evaluate the model at each training input point\nresponse = f(design[:,0], design[:,1], design[:,2])\n\n# train an emulator\nscalar_gasp = ScalarGaSP(ndim)\nscalar_gasp.train(design, response)\n\n# the trained emulator needs to be validated before moving on\n# here we leave out this step for simplicity...\n\n# draw samples of input for Sobol' analysis\ncalc_second_order = False\nnbase = 1024\n\nsaltelli_sampler = Saltelli(ndim, bounds, calc_second_order)\nsaltelli_samples = saltelli_sampler.sample(nbase)\n\n# evaluate the emulator to obtain approximated model output.\n# in order to take emulator-induced uncertainty into account, we draw 50 realizations\n# from the emulator\nY = scalar_gasp.sample(saltelli_samples, 50)\n\n# Compute Sobol' indices based on approximated model outputs\nsobol_analyzer = SobolAnalyze(ndim, Y, calc_second_order, seed=seed)\nS_res = sobol_analyzer.run(mode='parallel', max_workers=5)\n\nprint(\"\\n\")\nprint(\"Emulator-based first-order Sobol' index: \", S_res['S1'][:,0])\nprint(\"Analytical solution: [0.314, 0.442, 0]\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The following figure shows how the results of emulator-based Sobol' analysis\nlook like.\n\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import matplotlib.pyplot as plt\n\nname_vars = [r'$x_1$',r'$x_2$',r'$x_3$']\nx = {'S1':[1,4,7], 'ST':[2,5,8]}\ncolor = {'S1':'#69b3a2', 'ST':'#3399e6'}\nwidth=0.8\ncapsize=3\n\nfig, ax1 = plt.subplots(figsize=(4,3))\n\nax1.bar(x['S1'], S_res['S1'][:,0], width=width, yerr=S_res['S1'][:,2], color=color['S1'], capsize=capsize)\nax1.axis([0,9,0,0.7])\nax1.set_xticks([1.5,4.5,7.5])\nax1.set_xticklabels(name_vars)\nax1.tick_params(axis='both',bottom=False)\nax1.tick_params(axis=\"y\", labelcolor=color['S1'])\nax1.yaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)\nax1.set_ylabel('First-order index', color=color['S1'])\n\nax1.scatter(x['S1'], [0.314, 0.442, 0], s=30, c='red', marker='*')\n\nax2 = ax1.twinx()\nax2.bar(x['ST'], S_res['ST'][:,0], width=width, yerr=S_res['ST'][:,2], color=color['ST'], capsize=capsize)\nax2.axis([0,9,0,0.7])\nax2.set_xticks([1.5,4.5,7.5])\nax2.tick_params(axis='both',bottom=False)\nax2.tick_params(axis=\"y\", labelcolor=color['ST'])\nax2.yaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)\nax2.set_ylabel('Total-effect index', color=color['ST'])\n\nplt.tight_layout()"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.9.12"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}