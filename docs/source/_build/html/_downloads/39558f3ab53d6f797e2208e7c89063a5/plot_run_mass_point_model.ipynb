{
  "cells": [
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "%matplotlib inline"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n# RunSimulator: Mass Point Model\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "This example shows how to run multiple MassPointModel simulations in serial\nand parallelly using :class:`.RunSimulator`.\n\n\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "First, we import the class :class:`.MassPointModel` and define a `simulator`\nbased on :py:meth:`.MassPointModel.run` method.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "from psimpy.simulator import MassPointModel\n\nmpm = MassPointModel()\nmpm_simulator = mpm.run"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "This simulator takes required input data, solves the mass point model, and\nreturns time history of the mass point's location and velocity\n(see :py:meth:`.MassPointModel.run`).\n\nRequired inputs for `mpm_simulator` include:\n    1. topographic data: digital elevation model (in `ESRI ascii` format)\n    2. friction coefficients: coulomb friction and turbulent friction coefficients\n    3. initial state: initial location and initial velocity of the masspoint\n    4. computational parameters: such as time step, end time, etc.\n\nThe synthetic topography ``synthetic_topo.asc`` is used here for illustration.\nIt is located at the `/tests/data/` folder.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import os\n\ndir_data = os.path.abspath('../../../tests/data/')\nelevation = os.path.join(dir_data, 'synthetic_topo.asc')"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "<div class=\"alert alert-info\"><h4>Note</h4><p>You may need to modify ``dir_data`` according to where you save\n   ``synthetic_topo.asc`` on your local machine.</p></div>\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "For this example, we are going to run multiple simulations at different values\nof coulomb friction coefficient (``coulomb_friction``) and turbulent friction\ncoefficient (``turbulent_friction``) while keep other inputs fixed. Namely,\n``coulomb_friction`` and ``turbulent_friction`` are variable input parameters.\nAll other inputs are fixed input parameters.\n\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import numpy as np\nimport itertools\n\n# Variable iput parameters are defined as a list of strings. Their values will be\n# passed as a numpy array (var_samples) when run simulations.\nvar_inp_parameter = ['coulomb_friction', 'turbulent_friction']\ncoulomb_friction = np.arange(0.1, 0.31, 0.1)\nturbulent_friction = np.arange(500, 2001, 400)\nvar_samples = np.array(\n    [x for x in itertools.product(coulomb_friction, turbulent_friction)])\nprint(\"Number of variable input samples are: \", len(var_samples))\n\n# Fixed input parameters are defined as a dictionary. Their values are given.\nfix_inp = {'elevation': elevation, 'x0': 200, 'y0': 2000, 'tend': 50}"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "<div class=\"alert alert-info\"><h4>Note</h4><p>Parameters of `mpm_simulator` which are not included in\n   ``var_inp_parameter`` and ``fix_inp``, such as ``ux0``, ``uy0`` and\n   ``curvature``, will automatically take their default values.</p></div>\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "We may want to save outputs returned by `mpm_simulator` at each simulation for\nlater inspection or processing. In that case, we need to define ``dir_out`` and\nset ``save_out`` as `True`.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import os\n\n# Here we create a folder called `temp_run_MassPointModel_example` to save outputs\n# returned at each simulation.\ncwd = os.getcwd()\nif not os.path.exists('temp_run_MassPointModel_example'):\n    os.mkdir('temp_run_MassPointModel_example')\ndir_out = os.path.join(cwd, 'temp_run_MassPointModel_example')"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Now we can define an object of :class:`.RunSimulator` by\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "from psimpy.simulator import RunSimulator\n\nrun_mpm_simulator = RunSimulator(mpm_simulator, var_inp_parameter, fix_inp,\n    dir_out=dir_out, save_out=True)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "<div class=\"alert alert-info\"><h4>Note</h4><p>Since outputs of each simulation are saved in the same folder ``dir_out``,\n   we need to give each file a unique name in order to avoid conflict. This is\n   realized by defining ``prefixes``.</p></div>\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "serial_prefixes = [\"serial\"+str(i) for i in range(len(var_samples))]\nparallel_prefixes = [\"parallel\"+str(i) for i in range(len(var_samples))]"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Using :py:meth:`.RunSimulator.serial_run` method or\n:py:meth:`.RunSimulator.parallel_run` method, we can run the simulations\nin serial or parallelly.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import time\n\nstart = time.time()\nrun_mpm_simulator.serial_run(var_samples=var_samples, prefixes=serial_prefixes)\nserial_time = time.time() - start\nserial_output = run_mpm_simulator.outputs\n\nstart = time.time()\n# max_workers controls maximum number of tasks running in parallel\nrun_mpm_simulator.parallel_run(var_samples, prefixes=parallel_prefixes, max_workers=4)\nparallel_time = time.time() - start\nparallel_output = run_mpm_simulator.outputs\n    \nprint(\"Serial run time: \", serial_time)\nprint(\"Parallel run time: \", parallel_time)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Once a simulation is done, its output is saved to ``dir_out``. All output files\ngenerated by above simulations are as follows: \n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "os.listdir(dir_out)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Once all simulations are done, their outputs can also be accessed by\n:py:attr:`.RunSimulator.outputs` attribute, which is a list.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "# Here we print the maximum velocity of each simulation in the parallel run.\nmax_v = [np.max(output[:,5]) for output in parallel_output]\nprint(\n    f\"Maximum velocity of {parallel_prefixes[0]} to {parallel_prefixes[-1]} are: \",\n    '\\n',\n    max_v)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "<div class=\"alert alert-danger\"><h4>Warning</h4><p>If one simulation failed due to whatever reason, the error massage\n   will be printed to the screen but other simulations will continue. In that \n   case, the output file of failed simulation will not be writted to ``dir_out``.\n   Also, the element of :py:attr:`.RunSimulator.outputs` corresponding to that\n   simulation will be a string representing the error message, instead of a\n   numpy array.</p></div>\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Here we delete the folder `temp_run_MassPointModel_example` and all files therein.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import shutil\n\nshutil.rmtree(dir_out)"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.9.12"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}