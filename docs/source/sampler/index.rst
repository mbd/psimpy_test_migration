*******
Sampler
*******

This module hosts functionality for sampling methods.

Currently implemented classes are:

* :class:`.LHS`: Latin hypercube sampling.

* :class:`.MetropolisHastings`: Metropolis Hastings sampling.

* :class:`.Saltelli`: Saltelli's version of Sobol' sampling.

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Sampler

    Latin Hypercube <latin>
    Metropolis Hastings <metropolis_hastings>
    Saltelli <saltelli>