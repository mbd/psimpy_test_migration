Example Gallery
===============

.. toctree::

   Emulator <../auto_examples/emulator/index>
   Inference <../auto_examples/inference/index>
   Sampler <../auto_examples/sampler/index>
   Sensitivity <../auto_examples/sensitivity/index>
   Simulator <../auto_examples/simulator/index>
   